#define PI 3.1415
kernel void init_bitmap(global unsigned char* restrict img, const int size, const int channels, const float3 threshold, global bool* restrict bitmap)
{
	const int i = channels * get_global_id(0);
	if(i > size)
	return;

	if(img[i] >= threshold.x || img[i + 1] >= threshold.y || img[i + 2] >= threshold.z)
	bitmap[i/channels] = 1;
	else bitmap[i/channels] = 0;
}

kernel void init_data(global float* restrict data, global float* restrict buffer, const int width, const int min_north, const int min_east, const int size)
{
	const int i = 3 * get_global_id(0);
	if(i + 2 >= size)
	return;

	const float3 params = {data[i], data[i+1], data[i+2]};
	const int coord_x = params.x - min_east; 
	const int coord_y = params.y - min_north;
	buffer[coord_y * width + coord_x] = params.z;
}

kernel void generate_dir(global float3* restrict data, float focal, const int width, const int height, const double fow_tan, const float aspect_ratio)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);

	if(i >= width || j >= height) return;

	float norm_y = (i + 0.5f)/ width;
	float norm_z = (j + 0.5f)/ height;

	norm_y = (1 - 2.0f * norm_y) * aspect_ratio * fow_tan;
	norm_z = (1 - 2.0f * norm_z) * fow_tan;

	data[j * width + i] = (float3){-focal, norm_y, norm_z};
}

kernel void normalize_dir(global float3* data, const int width, const int height)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);

	if (i >= width || j >= height)
	return;

	float3 coords = data[j * width + i];

	const float magnitude = (float)	sqrt(coords.x * coords.x + coords.y * coords.y + coords.z * coords.z);
	coords = (float3){coords.x / magnitude, coords.y / magnitude, coords.z / magnitude};

	data[j * width + i] = coords;
}

kernel void apply_rotation(global float3* restrict data, const int width, const int height, const float3 sin, const float3 cos)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);

	if(i >= width || j >= height) return;

	const float3 coords = data[j*width + i];
	float3 temp_rot = (float3) {0, 0, 0};
	float _x, _y, _z;

	//Rotazione asse x
	_y = coords.y * cos.x + coords.z * sin.x;  
	_z = coords.y * -sin.x + coords.z * cos.x;  

	temp_rot.y += _y - coords.y;
	temp_rot.z += _z - coords.z;

	//Rotazione asse y
	_x = coords.x * cos.y + coords.y * sin.y;  
	_y = coords.x * -sin.y + coords.y * cos.y;  

	temp_rot.x += _x - coords.x;
	temp_rot.y += _y - coords.y;

	//Rotazione asse z
	_x = coords.x * cos.z + coords.z * -sin.z;  
	_z = coords.x * sin.z + coords.z * cos.z;  

	temp_rot.x += _x;
	temp_rot.y += coords.y;
	temp_rot.z += _z;

	data[j * width + i] = temp_rot;
}

kernel void find_start(float3 origin, global float3* restrict direction, global double* restrict start_points, int width, int height, int buf_width, int buf_height, float max_altitude)
{
	const int i = get_global_id(0);
	const int j = get_global_id(1);
	if(i >= width || j >= height) return;

	const float3 dir = direction[j * width + i];
	float tempX;
	float tempY;
	float tempZ;
	double min_start = 1;

	if(origin.x < 0 || origin.x > buf_width - 1)
	{
		tempX = -origin.x/dir.x;
		min_start = max(tempX, (float)min_start);
	}

	if(origin.y < 0 || origin.y > buf_height - 1)
	{
		tempY = -origin.y/dir.y;
		min_start = max(tempY, (float)min_start);
	}
	
	if(origin.z > max_altitude - 1){
		tempZ = -(origin.z - max_altitude)/dir.z;
		min_start = max(tempZ, (float)min_start);
	}

	start_points[j * width + i] = min_start;
}

bool exit_condition (float3 vec, int buf_width, int buf_height, float max_altitude)
{
	if(vec.x <= 0 || vec.x >= buf_width - 1 || vec.y <= 0 || vec.y >= buf_height-1 || vec.z < 0 || vec.z > max_altitude) return 1;
	return 0;
}

void cast(float3 vec, float3 dir, global float* restrict data_buffer, global bool* restrict bitmap, int bit_index, int buf_width, int buf_height, float max_altitude)
{
	while(1)
	{
		if(exit_condition(vec, buf_width, buf_height, max_altitude))
		{
			bitmap[bit_index] = 0;
			break;
		}
		int index = ((int) (vec.y + 0.5) * buf_width + (int) (vec.x + 0.5));
		if(data_buffer[index] > vec.z)
		{
			bitmap[bit_index] = 1;
			break;
		}
		vec = (float3) {vec.x + dir.x * 10.0f, vec.y + dir.y * 10.0f, vec.z + dir.z * 10.0f};
	}
}

kernel void ray_cast(float3 origin, global float3* restrict direction, global bool* restrict bitmap, global float* restrict data_buffer, int width, int height, int buf_width, int buf_height,
	float max_altitude, global double* restrict start_points)
	{
	const int i = 4 * get_global_id(0);
	const int j = get_global_id(1);

	if(i >= width || j >= height || j * width + i + 3 >= width * height)return;

	const double4 m = (double4) {start_points[j * width + i], start_points[j * width + i + 1], start_points[j * width + i + 2], start_points[j * width + i + 3]};
	float16 dir = (float16)	{direction[j * width + i].x, direction[j * width + i].y, direction[j * width + i].z, 0,
							 direction[j * width + i + 1].x, direction[j * width + i + 1].y, direction[j * width + i + 1].z, 0,
							 direction[j * width + i + 2].x, direction[j * width + i + 2].y, direction[j * width + i + 2].z, 0, 
							 direction[j * width + i + 3].x, direction[j * width + i + 3].y, direction[j * width + i + 3].z, 0};

	float3 vec = (float3) {dir.s0 * m.s0 + origin.x + 0.1f, dir.s1 * m.s0 + origin.y + 0.1f, dir.s2 * m.s0 + origin.z};
	cast(vec, dir.s012, data_buffer, bitmap, j * width + i, buf_width, buf_height, max_altitude);

	vec = (float3) {dir.s4 * m.s1 + origin.x + 0.1f, dir.s5 * m.s1 + origin.y + 0.1f, dir.s6 * m.s1 + origin.z};
	cast(vec, dir.s456, data_buffer, bitmap, j * width + i + 1, buf_width, buf_height, max_altitude);

	vec = (float3) {dir.s8 * m.s2 + origin.x + 0.1f, dir.s9 * m.s2 + origin.y + 0.1f, dir.sa * m.s2 + origin.z};
	cast(vec, dir.s89a, data_buffer, bitmap, j * width + i + 2, buf_width, buf_height, max_altitude);

	vec = (float3){dir.sc * m.s3 + origin.x + 0.1f, dir.sd * m.s3 + origin.y + 0.1f, dir.se * m.s3 + origin.z};
	cast(vec, dir.scde, data_buffer, bitmap, j * width + i + 3, buf_width, buf_height, max_altitude);
}

kernel void compare_bitmap(global bool* restrict bitmap1, global bool* restrict bitmap2, int size, global int* restrict compare)
{
	const int i = get_global_id(0);
	
	if(i > size) return;

	if(bitmap1[i] != bitmap2[i]) atomic_add(&compare[0], 1);
}

void cast_map(float3 vec, float3 dir, global float* restrict data_buffer, global int* restrict map, int map_index, int buf_width, int buf_height, float max_altitude)
{
	while(1)
	{
		if(exit_condition(vec, buf_width, buf_height, max_altitude))
		{
			map[map_index] = 0;
			break;
		}
		int index = ((int) (vec.y + 0.5) * buf_width + (int) (vec.x + 0.5));
		if(data_buffer[index] > vec.z)
		{
			map[map_index] = index;
			break;
		}
		vec = (float3) {vec.x + dir.x * 10.0f, vec.y + dir.y * 10.0f, vec.z + dir.z * 10.0f};
	}
}

kernel void ray_map(float3 origin, global float3* restrict direction, global int* restrict map, global float* restrict data_buffer, int width, int height, int buf_width, int buf_height,
	float max_altitude, global double* restrict start_points)
	{
	const int i = 4 * get_global_id(0);
	const int j = get_global_id(1);

	if(i >= width || j >= height || j * width + i + 3 >= width * height)return;

	const double4 m = (double4) {start_points[j * width + i], start_points[j * width + i + 1], start_points[j * width + i + 2], start_points[j * width + i + 3]};
	float16 dir = (float16)	{direction[j * width + i].x, direction[j * width + i].y, direction[j * width + i].z, 0,
							 direction[j * width + i + 1].x, direction[j * width + i + 1].y, direction[j * width + i + 1].z, 0,
							 direction[j * width + i + 2].x, direction[j * width + i + 2].y, direction[j * width + i + 2].z, 0, 
							 direction[j * width + i + 3].x, direction[j * width + i + 3].y, direction[j * width + i + 3].z, 0};

	float3 vec = (float3) {dir.s0 * m.s0 + origin.x + 0.1f, dir.s1 * m.s0 + origin.y + 0.1f, dir.s2 * m.s0 + origin.z};
	cast_map(vec, dir.s012, data_buffer, map, j * width + i, buf_width, buf_height, max_altitude);

	vec = (float3) {dir.s4 * m.s1 + origin.x + 0.1f, dir.s5 * m.s1 + origin.y + 0.1f, dir.s6 * m.s1 + origin.z};
	cast_map(vec, dir.s456, data_buffer, map, j * width + i + 1, buf_width, buf_height, max_altitude);

	vec = (float3) {dir.s8 * m.s2 + origin.x + 0.1f, dir.s9 * m.s2 + origin.y + 0.1f, dir.sa * m.s2 + origin.z};
	cast_map(vec, dir.s89a, data_buffer, map, j * width + i + 2, buf_width, buf_height, max_altitude);

	vec = (float3){dir.sc * m.s3 + origin.x + 0.1f, dir.sd * m.s3 + origin.y + 0.1f, dir.se * m.s3 + origin.z};
	cast_map(vec, dir.scde, data_buffer, map, j * width + i + 3, buf_width, buf_height, max_altitude);
}