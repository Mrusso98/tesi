#include <random>
#include <chrono>
#define NUM_OF_SAMPLES 5

typedef struct {
	cl_float2 x;
	cl_float2 y;
	cl_float2 z;
	cl_float2 fow;
}params_extremes;

bool verify_convergence(float x, float y) {
	if (y - x <= 0.001) return true;
	return false;
}

/*void ParamInit(parameters* p1, params_extremes* my_params, float temp_fow, int start, int samples) {
	float stridex = fabs(my_params->x.s1 - my_params->x.s0);
	float stridey = fabs(my_params->y.s1 - my_params->y.s0);
	float stridez = fabs(my_params->z.s1 - my_params->z.s0);
	float stridefow = fabs(my_params->fow.s1 - my_params->fow.s0);
	
	float dis[5];
	static std::random_device rd;
	static std::default_random_engine gen(rd());
	static std::normal_distribution<float> distribution(temp_fow / stridefow * (float)RAND_MAX, 8.0 / stridefow * (float)RAND_MAX);
	for (int i = 0; i < samples - start; i++) {
		dis[start + i] = fabs(distribution(gen));
	}

	for (int i = start; i < samples; i++) {
		p1[i].rot_x = my_params->x.s0 + (rand() / (float)RAND_MAX) * stridex;
		p1[i].rot_y = (int)(my_params->y.s0 + (rand() / (float)RAND_MAX) * stridey) % 360;
		p1[i].rot_z = my_params->z.s0 + (rand() / (float)RAND_MAX) * stridez;
		p1[i].fow = my_params->fow.s0 + (dis[i] / (float)RAND_MAX) * stridefow;
		p1[i].result = -1;
	}
}*/

void ParamInit(parameters* p1, params_extremes* my_params, float temp_fow, int start, int samples) {
	float stridex = fabs(my_params->x.s1 - my_params->x.s0);
	float stridey = fabs(my_params->y.s1 - my_params->y.s0);
	float stridez = fabs(my_params->z.s1 - my_params->z.s0);
	float stridefow = fabs(my_params->fow.s1 - my_params->fow.s0);

	float dis[5][4];
	static std::random_device rd;
	static std::default_random_engine gen(rd());
	static std::normal_distribution<float> distributionX((stridex/2) / stridex * (float)RAND_MAX, (stridex/6) / stridex * (float)RAND_MAX);
	static std::normal_distribution<float> distributionY((stridey/2) / stridey * (float)RAND_MAX, (stridey/3) / stridey * (float)RAND_MAX);
	static std::normal_distribution<float> distributionZ((stridez/2) / stridez * (float)RAND_MAX, (stridez/4) / stridez * (float)RAND_MAX);
	static std::normal_distribution<float> distributionFOW(temp_fow / stridefow * (float)RAND_MAX, 8.0 / stridefow * (float)RAND_MAX);

	for (int i = 0; i < samples - start; i++) {
		dis[start + i][0] = fabs(distributionX(gen));
		dis[start + i][1] = fabs(distributionY(gen));
		dis[start + i][2] = fabs(distributionZ(gen));
		dis[start + i][3] = fabs(distributionFOW(gen));
	}

	for (int i = start; i < samples; i++) {
		p1[i].rot_x = my_params->x.s0 + (dis[i][0] / (float)RAND_MAX) * stridex;
		p1[i].rot_y = (int)(my_params->y.s0 + (dis[i][1] / (float)RAND_MAX) * stridey) % 360;
		p1[i].rot_z = my_params->z.s0 + (dis[i][2] / (float)RAND_MAX) * stridez;
		p1[i].fow = my_params->fow.s0 + (dis[i][3] / (float)RAND_MAX) * stridefow;
		p1[i].result = -1;
	}
}
void RedefineParams(params_extremes* my_params, parameters p1) {
	float stridex = fabs(my_params->x.s1 - my_params->x.s0);
	float stridey = fabs(my_params->y.s1 - my_params->y.s0);
	float stridez = fabs(my_params->z.s1 - my_params->z.s0);
	float stridefow = fabs(my_params->fow.s1 - my_params->fow.s0);

	my_params->x.s1 = p1.rot_x + stridex / 4.0 >= my_params->x.s1? my_params->x.s1 : p1.rot_x + stridex / 4.0;
	my_params->x.s0 = p1.rot_x - stridex / 4.0 <= my_params->x.s0? my_params->x.s0 : p1.rot_x - stridex / 4.0;
	my_params->y.s1 = p1.rot_y + stridey / 4.0 >= my_params->y.s1 ? my_params->y.s1 : p1.rot_y + stridey / 4.0;
	my_params->y.s0 = p1.rot_y - stridey / 4.0 <= my_params->y.s0 ? my_params->y.s0 : p1.rot_y - stridey / 4.0;
	my_params->z.s1 = p1.rot_z + stridez / 4.0 >= my_params->z.s1 ? my_params->z.s1 : p1.rot_z + stridez / 4.0;
	my_params->z.s0 = p1.rot_z - stridez / 4.0 <= my_params->z.s0 ? my_params->z.s0 : p1.rot_z - stridez / 4.0;
	my_params->fow.s1 = p1.fow + stridefow / 2.0 >= my_params->fow.s1 ? my_params->fow.s1 : p1.fow + stridefow / 2.0;
	my_params->fow.s0 = p1.fow - stridefow / 2.0 <= my_params->fow.s0 ? my_params->fow.s0 : p1.fow - stridefow / 2.0;
}

params_extremes* FindExtremes(params_extremes* my_params, float originX, float originY, float width, float height) {
	float truemin = 0, truemax = 360;
	if (originX < 0) {
		if (originY < 0) {
			truemin = 0;
			truemax = 90;
		}
		else if (originY > height) {
			truemin = -90;
			truemax = 0;
		}
		else {
			truemin = -90;
			truemax = 90;
		}
	}
	else if (originX > width) {
		if (originY < 0) {
			truemin = 90;
			truemax = 180;
		}
		else if (originY > height) {
			truemin = 180;
			truemax = 270;
		}
		else {
			truemin = 90;
			truemax = 270;
		}
	}
	else {
		if (originY < 0) {
			truemin = 0;
			truemax = 180;
		}
		else if (originY > height) {
			truemin = 180;
			truemax = 360;
		}
	}

	my_params->y.s0 = truemin;
	my_params->y.s1 = truemax;
	return my_params;
}

void ParamOrder(parameters p1[], int num_of) {
	float min = 100;
	int index = -1;

	for (int i = 0; i < num_of - 1; i++) {
		min = p1[i].result;
		index = i;
		for (int j = i + 1; j < num_of; j++) {
			if (p1[j].result < min) {
				index = j;
				min = p1[j].result;
			}
		}
		parameters temp = p1[i];
		p1[i] = p1[index];
		p1[index] = temp;
	}
}

parameters FindMidpoint(parameters p1, parameters p2, params_extremes my_params) {
	parameters temp;
	temp.rot_x = (p1.rot_x + p2.rot_x) / 2.0;
	temp.rot_y = (p1.rot_y + p2.rot_y) / 2.0;
	temp.rot_z = (p1.rot_z + p2.rot_z) / 2.0;
	temp.fow = (p1.fow + p2.rot_x) / 2.0;
	temp.result = -1;
	return temp;
}

parameters AddParameters(parameters p1, parameters p2, params_extremes my_params) {
	parameters temp;
	temp.rot_x = p1.rot_x + p2.rot_x; 
	temp.rot_y = p1.rot_y + p2.rot_y;
	temp.rot_z = p1.rot_z + p2.rot_z;
	temp.fow = (p1.fow + p2.fow);
	return temp;
}

parameters SubParameters(parameters p1, parameters p2, params_extremes my_params) {
	parameters temp;
	temp.rot_x = p1.rot_x - p2.rot_x;
	temp.rot_y = p1.rot_y - p2.rot_y;
	temp.rot_z = p1.rot_z - p2.rot_z;
	temp.fow = (p1.fow - p2.fow);
	return temp;
}

parameters MulParameters(parameters p1, float x, params_extremes my_params) {
	parameters temp;
	temp.rot_x = p1.rot_x * x;	
	temp.rot_y = p1.rot_y * x;	
	temp.rot_z = p1.rot_z * x;
	temp.fow = p1.fow * x;
	return temp;
}

parameters FindCentroid(parameters* p, float x, params_extremes my_params) {
	parameters temp = p[0];
	for (int i = 1; i < x; i++) {
		temp = AddParameters(temp, p[i], my_params);
	}
	float div = 1.0 / x;
	temp = MulParameters(temp, div, my_params);
	return temp;
}

void PrintParameters(parameters p1) {
	printf("Rot [%.2f %.2f %.2f] - FoW [%.2f] ", p1.rot_x, p1.rot_y, p1.rot_z, p1.fow);
	if (p1.result >= 0)
		printf("- Result [%.3f]\n", p1.result);
	else
		printf("\n");
}

bool CompareParameters(parameters p1, params_extremes my_params) {
	if (p1.rot_x > my_params.x.s1 || p1.rot_x < my_params.x.s0) return 0;
	if (p1.rot_y > my_params.y.s1 || p1.rot_y < my_params.y.s0) return 0;
	if (p1.rot_z > my_params.z.s1 || p1.rot_z < my_params.z.s0) return 0;
	if (p1.fow > my_params.fow.s1 || p1.fow < my_params.fow.s0) return 0;
	return 1;
}
	