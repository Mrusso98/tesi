#ifndef VECTORS_H
#define VECTORS_H

struct vec
{
	float* data;
	size_t capacity; /* total capacity */
	size_t size; /* number of elements in vector */
};

void vector_init(struct vec* v, size_t init_capacity)
{
	v->data = (float*) malloc(init_capacity * sizeof(float));
	if (!v->data) return;

	v->size = 0;
	v->capacity = init_capacity;
}

void vector_push_back(struct vec* v, float elem) {
	if (v->size < v->capacity - 1) v->data[v->size] = elem;
	else{
		float* tmp;
		size_t newsize = v->capacity * 1.5 * sizeof(float);
		if (tmp = (float*) realloc(v->data, newsize)) {
			v->data = tmp;
			v->data[v->size] = elem;
			v->capacity = (size_t) v->capacity * 1.5;
		}
		else {
			printf("Realloc failed!");
			return;
		}
	}
	v->size++;
}
#endif // !VECTORS_H