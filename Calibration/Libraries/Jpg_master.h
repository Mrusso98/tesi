#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image/stb_image_write.h"

unsigned char* bitmap_to_jpeg(bool* bitmap, int bitmap_size, int channels) {
	unsigned char* img_jpeg = (unsigned char*)malloc(sizeof(unsigned char) * bitmap_size * channels);

	for (int i = 0; i < bitmap_size; i++) {
		if (bitmap[i] == 0) {
			for (int j = 0; j < channels; j++)
				img_jpeg[i * channels + j] = 0;
		}
		else if (bitmap[i] == 1) {
			for (int j = 0; j < channels; j++)
				img_jpeg[i * channels + j] = 122;
		}
	}
	return img_jpeg;
}

unsigned char* load_img(char* name, int *width, int *height, int *channels) {
	unsigned char* img = stbi_load(name, width, height, channels, 0);
	if (img == NULL) {
		printf("Error in loading the image\n");
		return NULL;
	}
	printf("Loaded image with a width of %dpx, a height of %dpx and %d channels\n", *width, *height, *channels);
	return img;
}
