#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

typedef struct {
	float focal_point;
	cl_float3 origin;
	cl_float3 rotation;
	float fow;
} Camera;

typedef struct {
	float rot_x;
	float rot_y;
	float rot_z;
	float fow;
	float result;
} parameters;

void CamInit(Camera* cam, float _focal, cl_float3 _origin, cl_float3 _rotation, float _fow) {
	cam->focal_point = _focal;
	cam->origin = _origin;
	cam->rotation = _rotation;
	cam->fow = _fow;
}


