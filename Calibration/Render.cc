#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <utility>
#include <set>

#define CL_TARGET_OPENCL_VERSION 120
#define NUM_OF_SAMPLES 5
#define TOLERANCE 0.015
#define PI 3.1415
#define ROT_CONST 57.2888f
#define MAX_ITER 200
#define MAX_RESET 3

#include "libraries/ocl_boiler.h" //Boilerplate OpenCL
#include "libraries/Jpg_master.h" //Legge e scrive immagini, gestisce le bitmap
#include "libraries/Dem_master.h" //Effettua parsing del file DEM
#include "libraries/Camera.h" //Effettua confronti fra risultati host e device
#include "libraries/Nelder_Mead.h" //Funzioni helper per Nelder-Mead
#include "libraries/vectors.h" //Implementazione vettore C
#include "libraries/raster.h"

/*Struct che contiene diversi parametri d'esecuzione del programma*/
typedef struct {
	char image_name [256];
	char dem_filename [256];
	cl_float3 convert_threshold;
	size_t lws;
	params_extremes my_params;
	cl_float3 origins;
}exec_settings;

/*Legge file Settings.txt, inizializza parametri*/
void exec_settings_init(exec_settings* my_settings) {
	FILE* fp;
	fopen_s(&fp, "Settings.txt", "r");
	
	if (fp == NULL) {
		printf("File Settings.txt missing from directory.\n");
		exit(1);
	}

	int count = 0;
	char line[256];
	while (fgets(line, 256, fp)) {
		if (count == 0)	fscanf(fp, "%s", my_settings->image_name);
		if (count == 3) fscanf(fp, "%s", my_settings->dem_filename);
		if (count == 6)	fscanf(fp, "%f %f %f", &my_settings->convert_threshold.x, &my_settings->convert_threshold.y, &my_settings->convert_threshold.z);
		if (count == 9) fscanf(fp, "%zu", &my_settings->lws);
		if (count == 12) fscanf(fp, "%f %f", &my_settings->my_params.x.s0, &my_settings->my_params.x.s1);
		if (count == 15) fscanf(fp, "%f %f", &my_settings->my_params.y.s0, &my_settings->my_params.y.s1);
		if (count == 18) fscanf(fp, "%f %f", &my_settings->my_params.z.s0, &my_settings->my_params.z.s1);
		if (count == 21) fscanf(fp, "%f %f", &my_settings->my_params.fow.s0, &my_settings->my_params.fow.s1);
		if (count == 24) fscanf(fp, "%f %f %f", &my_settings->origins.x, &my_settings->origins.y, &my_settings->origins.z);
		count++;
	}
}

/*Variabili per allineamento efficiente*/
size_t gws_align_init_bitmap;
size_t gws_align_init_data;
size_t gws_align_direction;
size_t gws_align_rotation;
size_t gws_align_points;
size_t gws_align_ray;
size_t gws_align_compare;

/*Trasforma .jpg di input in bitmap*/
cl_event init_bitmap(cl_kernel init_k, cl_command_queue que, cl_mem d_img, const cl_int size, const cl_int channels, cl_float3 threshold, cl_mem d_bitmap) {
	const size_t gws[] = {round_mul_up(size, gws_align_init_bitmap)};

	cl_int err;
	cl_event initbitmap_evt;
	cl_uint i = 0;
	err = clSetKernelArg(init_k, i++, sizeof(d_img), &d_img);
	ocl_check(err, "set parameter init failed", i - 1);
	err = clSetKernelArg(init_k, i++, sizeof(size), &size);
	ocl_check(err, "set parameter init failed", i - 1);
	err = clSetKernelArg(init_k, i++, sizeof(channels), &channels);
	ocl_check(err, "set parameter init failed", i - 1);
	err = clSetKernelArg(init_k, i++, sizeof(threshold), &threshold);
	ocl_check(err, "set parameter init failed", i - 1);
	err = clSetKernelArg(init_k, i++, sizeof(d_bitmap), &d_bitmap);
	ocl_check(err, "set parameter init failed", i - 1);

	err = clEnqueueNDRangeKernel(que, init_k, 1, NULL, gws, NULL, 0, NULL, &initbitmap_evt);
	ocl_check(err, "enqueue initbitmap failed");
	return initbitmap_evt;
}

/*Genera vettori direzione*/
cl_event generate_dir(cl_kernel dir_k, cl_command_queue que, cl_mem d_directions, cl_float focal, const cl_int width, const cl_int height, const cl_double fow_tan, const cl_float aspect_ratio) {
	const size_t gws[] = { round_mul_up(width, gws_align_direction), round_mul_up(height, gws_align_direction) };

	cl_int err;
	cl_event direction_evt;
	cl_uint i = 0;
	err = clSetKernelArg(dir_k, i++, sizeof(d_directions), &d_directions);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(focal), &focal);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(fow_tan), &fow_tan);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(aspect_ratio), &aspect_ratio);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, dir_k, 2, NULL, gws, NULL, 0, NULL, &direction_evt);
	ocl_check(err, "enqueue generate_dir failed");
	return direction_evt;
}

/*Normalizza vettori direzione*/
cl_event normalize(cl_kernel norm_k, cl_command_queue que, cl_mem d_directions, const cl_int width, const cl_int height) {
	const size_t gws[] = { round_mul_up(width, gws_align_direction), round_mul_up(height, gws_align_direction) };

	cl_int err;
	cl_event normalize_evt;
	cl_uint i = 0;
	err = clSetKernelArg(norm_k, i++, sizeof(d_directions), &d_directions);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(norm_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(norm_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, norm_k, 2, NULL, gws, NULL, 0, NULL, &normalize_evt);
	ocl_check(err, "enqueue normalize failed");
	return normalize_evt;
}

/*Applica matrici di rotazione al vettore*/
cl_event apply_rotation(cl_kernel rot_k, cl_command_queue que, cl_mem d_directions, const cl_int width, const cl_int height, const cl_float3 sin, const cl_float3 cos) {
	const size_t gws[] = { round_mul_up(width, gws_align_rotation), round_mul_up(height, gws_align_rotation) };
	cl_int err;
	cl_event rotation_evt;
	cl_uint i = 0;

	err = clSetKernelArg(rot_k, i++, sizeof(d_directions), &d_directions);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(sin), &sin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(cos), &cos);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, rot_k, 2, NULL, gws, NULL, 0, NULL, &rotation_evt);
	ocl_check(err, "enqueue apply_rotation failed");
	return rotation_evt;
}

/*Trova coefficienti angolari iniziali*/
cl_event find_start_points(cl_kernel points_k, cl_command_queue que, cl_float3 origin, cl_mem direction, cl_mem start_points, cl_int width, cl_int height, 
	cl_int buf_width, cl_int buf_height, cl_float max_altitude) {
	const size_t gws[] = { round_mul_up(width, gws_align_points), round_mul_up(height, gws_align_points)};
	cl_int err;
	cl_event points_evt;
	cl_uint i = 0;
	err = clSetKernelArg(points_k, i++, sizeof(origin), &origin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(direction), &direction);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(start_points), &start_points);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(buf_width), &buf_width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(buf_height), &buf_height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(max_altitude), &max_altitude);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, points_k, 2, NULL, gws, NULL, 0, NULL, &points_evt);
	ocl_check(err, "enqueue start_points failed");
	return points_evt;
}

/*Effettua raycasting*/
cl_event ray_cast(cl_kernel cast_k, cl_command_queue que, cl_float3 origin, cl_mem direction, cl_mem bitmap, cl_mem data_buffer, cl_int width, cl_int height,
	cl_int buffer_width, cl_int buffer_height, cl_float max_altitude, cl_mem start_points) {
	const size_t gws[] = { round_mul_up(width, gws_align_ray), round_mul_up(height, gws_align_ray) };
	cl_int err;
	cl_event cast_evt;
	cl_uint i = 0;
	err = clSetKernelArg(cast_k, i++, sizeof(origin), &origin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(direction), &direction);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(bitmap), &bitmap);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(data_buffer), &data_buffer);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(buffer_width), &buffer_width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(buffer_height), &buffer_height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(max_altitude), &max_altitude);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(cast_k, i++, sizeof(start_points), &start_points);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, cast_k, 2, NULL, gws, NULL, 0, NULL, &cast_evt);
	ocl_check(err, "enqueue ray_cast failed");
	return cast_evt;
}

cl_event ray_map(cl_kernel map_k, cl_command_queue que, cl_float3 origin, cl_mem direction, cl_mem map, cl_mem data_buffer, cl_int width, cl_int height,
	cl_int buffer_width, cl_int buffer_height, cl_float max_altitude, cl_mem start_points) {
	const size_t gws[] = { round_mul_up(width, gws_align_ray), round_mul_up(height, gws_align_ray) };
	cl_int err;
	cl_event map_evt;
	cl_uint i = 0;
	err = clSetKernelArg(map_k, i++, sizeof(origin), &origin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(direction), &direction);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(map), &map);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(data_buffer), &data_buffer);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(buffer_width), &buffer_width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(buffer_height), &buffer_height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(max_altitude), &max_altitude);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(start_points), &start_points);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, map_k, 2, NULL, gws, NULL, 0, NULL, &map_evt);
	ocl_check(err, "enqueue ray_map failed");
	return map_evt;
}

/*Confronta due bitmap*/
cl_event cl_compare_bitmap(cl_kernel compare_k, cl_command_queue que, cl_mem compare, cl_mem bitmap1, cl_mem bitmap2, cl_int size, size_t _lws) {
	const size_t gws[] = { round_mul_up(size, gws_align_compare)};

	cl_int err;
	cl_event compare_evt;
	cl_uint i = 0;
	err = clSetKernelArg(compare_k, i++, sizeof(bitmap1), &bitmap1);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(compare_k, i++, sizeof(bitmap2), &bitmap2);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(compare_k, i++, sizeof(size), &size);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(compare_k, i++, sizeof(compare), &compare);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, compare_k, 1, NULL, gws, NULL, 0, NULL, &compare_evt);
	ocl_check(err, "enqueue compare_bitmap failed");
	return compare_evt;
}

void pre_casting(cl_command_queue que, cl_context ctx, cl_kernel generate_dir_k, cl_kernel normalize_k, cl_kernel rotation_k, cl_kernel start_points_k, cl_mem d_data_buffer,
	cl_mem d_directions, cl_mem d_start_points, int vec_width, int vec_height, int width, int height, float max_altitude, Camera my_cam, cl_double3* runtimes, cl_int3* num_of_casts){
	cl_event read_evt;
	int err;
	cl_float3 _sin;
	cl_float3 _cos;

	/*Genera vettori direzione*/
	const cl_double fow_tan = tan((my_cam.fow / 2.0) * PI / 180.0);
	const cl_float aspect_ratio = width / (float)height;
	cl_event generate_dir_evt = generate_dir(generate_dir_k, que, d_directions, my_cam.focal_point, width, height, fow_tan, aspect_ratio);
	clWaitForEvents(1, &generate_dir_evt);
	runtimes->x += runtime_ms(generate_dir_evt);
	num_of_casts->x += 1;

	/*Applica rotazione*/
	_sin.x = sin(my_cam.rotation.x / ROT_CONST);
	_cos.x = cos(my_cam.rotation.x / ROT_CONST);
	_sin.y = sin(my_cam.rotation.y / ROT_CONST);
	_cos.y = cos(my_cam.rotation.y / ROT_CONST);
	_sin.z = sin(my_cam.rotation.z / ROT_CONST);
	_cos.z = cos(my_cam.rotation.z / ROT_CONST);
	cl_event apply_rotation_evt = apply_rotation(rotation_k, que, d_directions, width, height, _sin, _cos);
	clWaitForEvents(1, &apply_rotation_evt);
	runtimes->y += runtime_ms(apply_rotation_evt);
	num_of_casts->y++;

	/*Normalizza*/
	cl_event normalize_evt = normalize(normalize_k, que, d_directions, width, height);
	clWaitForEvents(1, &normalize_evt);

	/*Calcola coefficienti angolari*/
	cl_event start_points_evt = find_start_points(start_points_k, que, my_cam.origin, d_directions, d_start_points, width, height, vec_width, vec_height, max_altitude);
	clWaitForEvents(1, &start_points_evt);

	err = clFinish(que);
	clReleaseEvent(apply_rotation_evt);
	clReleaseEvent(normalize_evt);
	clReleaseEvent(start_points_evt);
}

/*Compie tutte le operazioni relative alla mappatura*/
bool* createBitmap(cl_command_queue que, cl_context ctx, cl_kernel generate_dir_k, cl_kernel normalize_k, cl_kernel rotation_k, cl_kernel start_points_k, cl_kernel ray_cast_k, 
	cl_mem d_data_buffer, cl_mem d_directions, cl_mem d_start_points, int vec_width, int vec_height, int width, int height, float max_altitude, Camera my_cam, cl_double3* runtimes, 
	cl_int3* num_of_casts) {
	cl_int err;
	cl_event read_evt;

	pre_casting(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, d_data_buffer, d_directions, d_start_points, vec_width, vec_height, width, height, max_altitude,
		my_cam, runtimes, num_of_casts);

	/*Ray casting*/
	cl_mem d_result_bitmap = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(bool) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_result_bitmap");
	cl_event ray_cast_evt = ray_cast(ray_cast_k, que, my_cam.origin, d_directions, d_result_bitmap, d_data_buffer, width, height, vec_width, vec_height, max_altitude, d_start_points);
	clWaitForEvents(1, &ray_cast_evt);
	runtimes->z += runtime_ms(ray_cast_evt);
	num_of_casts->z += 1;
	
	bool* dh_result_bitmap = (bool*)malloc(sizeof(bool) * width * height);
	err = clEnqueueReadBuffer(que, d_result_bitmap, CL_TRUE, 0, sizeof(bool) * width * height, dh_result_bitmap, 0, NULL, &read_evt);
	ocl_check(err, "errore nella creazione del buffer dh_result_bitmap");

	clReleaseMemObject(d_result_bitmap);
	clReleaseEvent(ray_cast_evt);
	clReleaseEvent(read_evt);
	return dh_result_bitmap;
}

/*Confronta le due bitmap*/
float bitmap_diff(cl_command_queue que, cl_context ctx, cl_kernel compare_k, cl_mem compare, cl_mem d_bitmap1, cl_mem d_bitmap2, 
	bool* bitmap1, bool* bitmap2, int width, int height, double* total_runtime_comparison, int _lws) {
	int err;
	int* h_compare = (int*) malloc(sizeof(int));
	const int zero = 0;
	int bitmap_size = width * height;

	err = clEnqueueFillBuffer(que, compare, &zero, sizeof(int), 0, sizeof(int), 0, NULL, NULL); 
	ocl_check(err, "errore nella creazione del buffer compare");
	err = clEnqueueWriteBuffer(que, d_bitmap1, CL_TRUE, 0, bitmap_size *sizeof(bool), bitmap1, 0, NULL, NULL);
	ocl_check(err, "errore nella scrittura del buffer d_bitmap1");
	err = clEnqueueWriteBuffer(que, d_bitmap2, CL_TRUE, 0, bitmap_size *sizeof(bool), bitmap2, 0, NULL, NULL);
	ocl_check(err, "errore nella scrittura del buffer d_bitmap2");
	
	cl_event compare_evt = cl_compare_bitmap(compare_k, que, compare, d_bitmap1, d_bitmap2, bitmap_size, _lws);

	clWaitForEvents(1, &compare_evt);
	*total_runtime_comparison += runtime_ms(compare_evt);

	err = clEnqueueReadBuffer(que, compare, CL_TRUE, 0, sizeof(int), h_compare, 0, NULL, NULL);

	float result = (float)h_compare[0] / bitmap_size;
	
	free(h_compare);
	clReleaseEvent(compare_evt);

	return result;
}

cl_int* mapping(cl_command_queue que, cl_context ctx, cl_kernel generate_dir_k, cl_kernel normalize_k, cl_kernel rotation_k, cl_kernel start_points_k, cl_kernel ray_map_k,
	cl_mem d_data_buffer, cl_mem d_directions, cl_mem d_start_points, int vec_width, int vec_height, int width, int height, float max_altitude, Camera my_cam, cl_double3* runtimes,
	cl_int3* num_of_casts) {
	cl_int err;
	cl_event read_evt;

	pre_casting(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, d_data_buffer, d_directions, d_start_points, vec_width, vec_height, width, height, max_altitude,
		my_cam, runtimes, num_of_casts);

	/*Ray casting*/
	cl_mem d_result_map = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_result_map");
	cl_event ray_map_evt = ray_map(ray_map_k, que, my_cam.origin, d_directions, d_result_map, d_data_buffer, width, height, vec_width, vec_height, max_altitude, d_start_points);
	clWaitForEvents(1, &ray_map_evt);
	runtimes->z += runtime_ms(ray_map_evt);
	num_of_casts->z += 1;

	cl_int* dh_result_map = (cl_int*)malloc(sizeof(cl_int) * width * height);
	err = clEnqueueReadBuffer(que, d_result_map, CL_TRUE, 0, sizeof(cl_int) * width * height, dh_result_map, 0, NULL, &read_evt);
	ocl_check(err, "errore nella creazione del buffer dh_result_map");
	
	clReleaseMemObject(d_result_map);
	clReleaseEvent(ray_map_evt);
	return dh_result_map;
}

float initFow(cl_float3 origins, int vec_height, int vec_width, float max_altitude) {
	float temp = FLT_MIN;
	float var;

	if (origins.x < 0 || origins.x >= vec_width || origins.y < 0 || origins.y >= vec_height || origins.z > max_altitude) {
		if (origins.x < 0) temp = -origins.x;
		else if (origins.x >= vec_width) temp = origins.x - vec_width;
		if (origins.y < 0) temp = max(temp, -origins.y);
		else if (origins.y >= vec_height) temp = max(temp, origins.y - vec_height);
		if (origins.z > max_altitude) temp = max(temp, origins.z - max_altitude);
		var = temp / 102.50;
	}
	else return 50;

	return min(50, (int)(90 - var + 8));
}

int main(int argc, char* argv[]) {
	srand(time(NULL));
	exec_settings my_settings;
	exec_settings_init(&my_settings);

	cl_platform_id p;
	cl_device_id d;
	cl_context ctx;
	cl_command_queue que;
	cl_program prog;

	/*Boilerplate OCL*/
	p = select_platform(0);
	d = select_device(p);
	ctx = create_context(p, d);
	que = create_queue(ctx, d);
	prog = create_program("CameraMap.ocl", ctx, d);

	cl_int err;

	/*Inizializza i kernel del codice device*/
	cl_kernel init_bitmap_k = clCreateKernel(prog, "init_bitmap", &err);
	ocl_check(err, "errore nella creazione del kernel init_bitmap");
	cl_kernel init_data_k = clCreateKernel(prog, "init_data", &err);
	ocl_check(err, "errore nella creazione del kernel init_bitmap");
	cl_kernel generate_dir_k = clCreateKernel(prog, "generate_dir", &err);
	ocl_check(err, "errore nella creazione del kernel generate_dir");
	cl_kernel normalize_k = clCreateKernel(prog, "normalize_dir", &err);
	ocl_check(err, "errore nella creazione del kernel normalize");
	cl_kernel rotation_k = clCreateKernel(prog, "apply_rotation", &err);
	ocl_check(err, "errore nella creazione del kernel apply_rotation");
	cl_kernel start_points_k = clCreateKernel(prog, "find_start", &err);
	ocl_check(err, "errore nella creazione del kernel start_points");
	cl_kernel ray_cast_k = clCreateKernel(prog, "ray_cast", &err);
	ocl_check(err, "errore nella creazione del kernel ray_cast_k");
	cl_kernel compare_bitmap_k = clCreateKernel(prog, "compare_bitmap", &err);
	ocl_check(err, "errore nella creazione del kernel compare_bitmap");
	cl_kernel ray_map_k = clCreateKernel(prog, "ray_map", &err);
	ocl_check(err, "errore nella creazione del kernel ray_map");

	/*Query sul device per ottenere i preferred work group size multiple di ogni kernel*/
	err = clGetKernelWorkGroupInfo(init_bitmap_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init_bitmap), &gws_align_init_bitmap, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(init_data_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init_data), &gws_align_init_data, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(generate_dir_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_direction), &gws_align_direction, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(rotation_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_rotation), &gws_align_rotation, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(start_points_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_points), &gws_align_points, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(ray_cast_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_ray), &gws_align_ray, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(compare_bitmap_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_compare), &gws_align_compare, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	
	Camera my_cam;

	/*Carica dati dell'immagine .jpg. Il nome � passato tramite parametro.*/
	int width, height, channels;
	unsigned char* img = load_img(my_settings.image_name, &width, &height, &channels);
	size_t img_size = width * height * channels;

	/*Crea buffer per la bitmap su device*/
	cl_mem d_bitmap, d_img, hd_vec, d_data_buffer;
	d_bitmap = clCreateBuffer(ctx, CL_MEM_READ_WRITE, width * height * sizeof(bool), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_bitmap");

	/*Crea copia device dell'immagine per creazione della bitmap*/
	d_img = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, img_size*sizeof(unsigned char), img, &err);
	ocl_check(err, "errore nella creazione del buffer d_img");

	cl_event create_d_bitmap_evt, init_data_evt, read_evt, write_evt;
	/*Lancia kernel creazione bitmap*/
	create_d_bitmap_evt = init_bitmap(init_bitmap_k, que, d_img, img_size, channels, my_settings.convert_threshold, d_bitmap);

	/*Copia risultato su host per confronto*/
	bool* dh_bitmap = (bool*)malloc(sizeof(bool) * img_size / channels);
	err = clEnqueueReadBuffer(que, d_bitmap, CL_TRUE, 0, sizeof(bool) * img_size / channels, dh_bitmap, 1, &create_d_bitmap_evt, &read_evt);
	ocl_check(err, "errore nella creazione del buffer dh_bitmap");
	unsigned char* img_2 = bitmap_to_jpeg(dh_bitmap, img_size / channels, channels);
	stbi_write_jpg("img_bitmap_2.jpg", width, height, channels, img_2, 100);

	clReleaseMemObject(d_bitmap);
	clReleaseMemObject(d_img);
	free(img_2);
	
	/*Parsing DEM .ENVI*/
	Raster dem(my_settings.dem_filename);
	float* dem_data = dem.get();

	float vec_width = dem.east - dem.west + 1;
	float vec_height = dem.north - dem.south + 1;
	float max_altitude = dem.minmax[0].second;
	double total_runtime_data = 0;

	long long num_of_els = parse_envi(dem_data, max_altitude, vec_width, vec_height, dem.south, dem.west, dem.north, dem.east, dem.rows, dem.cols, dem.vres, dem.hres,
		&d_data_buffer, &hd_vec, ctx, que, &write_evt, init_data_k, &init_data_evt, &total_runtime_data, gws_align_init_data);
	
	/*Parametri iniziali telecamera e benchmarking*/
	cl_float3 origins = {my_settings.origins.x - dem.west, my_settings.origins.y - dem.south, my_settings.origins.z};
	printf("Origins: %f %f %f\n", origins.x, origins.y, origins.z);
	cl_double3 runtimes = { 0, 0, 0 };
	cl_int3 num_of_casts = { 0, 0, 0 };

	/*Trova estremi iniziali*/
	FindExtremes(&my_settings.my_params, origins.x, origins.y, vec_width, vec_height);
	double total_runtime_comparison = 0;
	int num_of_comparisons = 0;

	float temp_minY;
	float temp_minZ;
	bool* temp_bitmap;
	bool* zero_bitmap = (bool*)malloc(sizeof(bool) * img_size/channels);
	memset(zero_bitmap, 0, sizeof(bool) * img_size / channels);
	bool end = false;
	
	float tempFOW = initFow(origins, vec_height, vec_width, max_altitude);

	/*Crea buffer OpenCL necessari per l'esecuzione*/
	cl_mem d_directions = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_float3) * img_size/channels, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_directions");

	cl_mem d_start_points = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_double) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_start_points");

	cl_mem compare = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(int), NULL, &err);
	ocl_check(err, "errore nella creazione del buffer compare");

	cl_mem d_bitmap1 = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(bool) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_bitmap1");

	cl_mem d_bitmap2 = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(bool) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_bitmap2");

	/*Precalibrazione per diminuire i range possibili. Step = 5 gradi.*/
	printf("Starting pre-calibration for Y axis..\n");

	cl_float3 temp_rot;
	float temp_res;

	for (temp_minY = my_settings.my_params.y.s0; temp_minY <= my_settings.my_params.y.s1 && !end; temp_minY += 5) {
		for (temp_minZ = my_settings.my_params.z.s0; temp_minZ <= my_settings.my_params.z.s1; temp_minZ += 5) {
			temp_rot = { 0, temp_minY, temp_minZ };
			CamInit(&my_cam, -1, origins, temp_rot, tempFOW);
			temp_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
				vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
			temp_res = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, temp_bitmap, zero_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
			num_of_comparisons++;

			if (temp_res > 0) {
				my_settings.my_params.y.s0 = temp_minY;
				end = true;
				break;
			}
		}
	}

	for (temp_minY = my_settings.my_params.y.s1; temp_minY >= my_settings.my_params.y.s0 && end; temp_minY -= 5) {
		for (temp_minZ = my_settings.my_params.z.s0; temp_minZ <= my_settings.my_params.z.s1; temp_minZ += 5) {
			temp_rot = { 0, temp_minY, temp_minZ };
			CamInit(&my_cam, -1, origins, temp_rot, tempFOW);
			temp_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
				vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
			temp_res = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, temp_bitmap, zero_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
			num_of_comparisons++;

			if (temp_res > 0) {
				my_settings.my_params.y.s1 = temp_minY;
				end = false;
				break;
			}
		}
	}

	printf("Starting pre-calibration for Z axis..\n");
	for (temp_minZ = my_settings.my_params.z.s0; temp_minZ <= my_settings.my_params.z.s1 && !end; temp_minZ += 5) {
		for (temp_minY = my_settings.my_params.y.s0; temp_minY <= my_settings.my_params.y.s1; temp_minY += 5) {
			temp_rot = { 0, temp_minY, temp_minZ };
			CamInit(&my_cam, -1, origins, temp_rot, tempFOW);
			temp_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
				vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
			temp_res = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, temp_bitmap, zero_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
			num_of_comparisons++;

			if (temp_res > 0) {
				my_settings.my_params.z.s0 = temp_minZ;
				end = true;
				break;
			}
		}
	}

	for (temp_minZ = my_settings.my_params.z.s1; temp_minZ >= my_settings.my_params.z.s0 && end; temp_minZ -= 5) {
		for (temp_minY = my_settings.my_params.y.s0; temp_minY <= my_settings.my_params.y.s1; temp_minY += 5) {
			temp_rot = { 0, temp_minY, temp_minZ };
			CamInit(&my_cam, -1, origins, temp_rot, tempFOW);
			temp_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
				vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
			temp_res = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, temp_bitmap, zero_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
			num_of_comparisons++;

			if (temp_res > 0) {
				my_settings.my_params.z.s1 = temp_minZ;
				end = false;
				break;
			}
		}
	}
	
	free(zero_bitmap);

	printf("\nExtremes:\n");
	printf("X : %f %f\n", my_settings.my_params.x.s0, my_settings.my_params.x.s1);
	printf("Y : %f %f\n", my_settings.my_params.y.s0, my_settings.my_params.y.s1);
	printf("Z : %f %f\n\n", my_settings.my_params.z.s0, my_settings.my_params.z.s1);
	printf("Starting Nelder-Mead..\n");

	/*Creazione simplesso*/
	parameters* comp_res = (parameters*)malloc(sizeof(parameters) * NUM_OF_SAMPLES);

	cl_float3 new_rot;
	bool* res_bitmap, *dh_result_bitmap;
	bool is_done = false;
	params_extremes temp_params = my_settings.my_params;
	int iter_counter, shrinking, reset_counter = 0;

	parameters best_result, C, R, E, IC, OC, temp;
	best_result.result = 1;

	/*Nelder-Mead. 5 reset massimi.*/
	for (int p = 0; p < 5 && !is_done; p++) {
		iter_counter = 1;
		shrinking = 1;
		if (p > 0) {
			PrintParameters(comp_res[0]);
			if (p == 3) {
				printf("Complete algorithm reset..\n");
				if (comp_res[0].result < best_result.result) best_result = comp_res[0];
				if (reset_counter++ == MAX_RESET) {
					is_done = true;
					printf("The algorithm has failed to find a result\n");
					printf("Printing best result. The difference between the current parameters and the ideal is %.2f %\n", best_result.result * 100);
					break;
				}
				p = 0;
				temp_params = my_settings.my_params;

				ParamInit(comp_res, &temp_params, tempFOW, 0, NUM_OF_SAMPLES);
			}
			/*Se � un reset, diminuisci range e resetta parametri*/
			RedefineParams(&temp_params, comp_res[0]);

			ParamInit(comp_res, &temp_params, tempFOW, 1, NUM_OF_SAMPLES);
		}
		else {
			/*Se non � un reset, inizializza da 0*/
			ParamInit(comp_res, &temp_params, tempFOW, 0, NUM_OF_SAMPLES);
		}
		
		printf("\nIteration number: %d\n", p + 1);
		/*Primi confronti del simplesso base*/
		for (int j = 0; j < NUM_OF_SAMPLES; j++) {
			new_rot = { comp_res[j].rot_x, comp_res[j].rot_y, comp_res[j].rot_z };
			CamInit(&my_cam, -1, origins, new_rot, comp_res[j].fow);

			dh_result_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
				vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
			comp_res[j].result = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, dh_result_bitmap, dh_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
			num_of_comparisons++;

			free(dh_result_bitmap);
			clFinish(que);
		}

		while (1) {
			//Stampa parametri e check vari.
			ParamOrder(comp_res, NUM_OF_SAMPLES);

			if (comp_res[0].result <= TOLERANCE) {
				best_result = comp_res[0];
				printf("\nFound a result..\n");
				is_done = true;
				break;
			}
			else if (verify_convergence(comp_res[0].result, comp_res[NUM_OF_SAMPLES-1].result)) {
				break;
			}
			else if (iter_counter == MAX_ITER) {
				break;
			}

			/*Compute R*/
			C = FindCentroid(comp_res, NUM_OF_SAMPLES, my_settings.my_params);
			R = SubParameters(C, comp_res[NUM_OF_SAMPLES - 1], my_settings.my_params);
			R = MulParameters(R, 1, my_settings.my_params);
			R = AddParameters(C, R, my_settings.my_params);
			if (!CompareParameters(R, my_settings.my_params)) { R.result = 1; }
			else {
				new_rot = { R.rot_x, R.rot_y, R.rot_z };
				CamInit(&my_cam, -1, origins, new_rot, R.fow);
				res_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
					vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
				R.result = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, res_bitmap, dh_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
				num_of_comparisons++;
			}

			if (comp_res[0].result <= R.result && R.result < comp_res[NUM_OF_SAMPLES - 2].result) {
				shrinking = 1;
				comp_res[NUM_OF_SAMPLES - 1] = R;
			}
			else if (R.result < comp_res[0].result) {
				shrinking = 1;
				/*Compute E*/
				E = SubParameters(R, C, my_settings.my_params);
				E = MulParameters(E, 2.0f, my_settings.my_params);
				E = AddParameters(C, E, my_settings.my_params);
				if (!CompareParameters(E, my_settings.my_params)) E.result = 1;
				else {
					new_rot = { E.rot_x, E.rot_y, E.rot_z };
					CamInit(&my_cam, -1, origins, new_rot, E.fow);
					res_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
						vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
					E.result = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, res_bitmap, dh_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
					num_of_comparisons++;
				}

				if (E.result < R.result) {
					comp_res[NUM_OF_SAMPLES - 1] = E;
				}
				else {
					comp_res[NUM_OF_SAMPLES - 1] = R;
				}
			}
			else if (R.result >= comp_res[NUM_OF_SAMPLES - 2].result && R.result < comp_res[NUM_OF_SAMPLES - 1].result) {
				shrinking = 1;

				OC = SubParameters(R, C, my_settings.my_params);
				OC = MulParameters(OC, 0.5f, my_settings.my_params);
				OC = AddParameters(C, OC, my_settings.my_params);

				if (!CompareParameters(OC, my_settings.my_params)) OC.result = 1;
				else {
					new_rot = { OC.rot_x, OC.rot_y, OC.rot_z };
					CamInit(&my_cam, -1, origins, new_rot, OC.fow);
					res_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
						vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
					OC.result = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, res_bitmap, dh_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
					num_of_comparisons++;
				}

				if (OC.result < comp_res[NUM_OF_SAMPLES - 1].result) {
					comp_res[NUM_OF_SAMPLES - 1] = OC;
				}
			}
			else if (R.result >= comp_res[NUM_OF_SAMPLES - 1].result) {
				IC = SubParameters(R, C, my_settings.my_params);
				IC = MulParameters(IC, 0.5f, my_settings.my_params);
				IC = SubParameters(C, IC, my_settings.my_params);
				if (!CompareParameters(IC, my_settings.my_params)) IC.result = 1;
				else {
					new_rot = { IC.rot_x, IC.rot_y, IC.rot_z };
					CamInit(&my_cam, -1, origins, new_rot, IC.fow);
					res_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
						vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
					IC.result = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, res_bitmap, dh_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
					num_of_comparisons++;
				}

				if (IC.result < comp_res[NUM_OF_SAMPLES - 1].result) {
					comp_res[NUM_OF_SAMPLES - 1] = IC;
					shrinking = 1;
				}
				else {
					temp = SubParameters(comp_res[NUM_OF_SAMPLES - shrinking], comp_res[0], my_settings.my_params);
					temp = MulParameters(temp, 0.5f, my_settings.my_params);
					temp = AddParameters(comp_res[0], temp, my_settings.my_params);
					if (!CompareParameters(temp, my_settings.my_params)) temp.result = 1;
					else {
						new_rot = { temp.rot_x, temp.rot_y, temp.rot_z };
						CamInit(&my_cam, -1, origins, new_rot, temp.fow);
						res_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points, 
							vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);
						temp.result = bitmap_diff(que, ctx, compare_bitmap_k, compare, d_bitmap1, d_bitmap2, res_bitmap, dh_bitmap, width, height, &total_runtime_comparison, my_settings.lws);
						num_of_comparisons++;
					}
					comp_res[NUM_OF_SAMPLES - shrinking] = temp;

					if (shrinking == NUM_OF_SAMPLES) shrinking = 1;
					else shrinking++;
				}
			}
			iter_counter++;
		}
	}
	
	free(comp_res);

	new_rot = { best_result.rot_x, best_result.rot_y, best_result.rot_z };
	CamInit(&my_cam, -1, origins, new_rot, best_result.fow);
	res_bitmap = createBitmap(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_cast_k, d_data_buffer, d_directions, d_start_points,
		vec_width, vec_height, width, height, max_altitude, my_cam, &runtimes, &num_of_casts);

	/*Stampa bitmap risultante dal raycasting*/
	unsigned char* img_3 = bitmap_to_jpeg(res_bitmap, img_size / channels, channels);
	stbi_write_jpg("result.jpg", width, height, channels, img_3, 100);
	free(res_bitmap);

	printf("\nRotation: (%f, %f, %f), FOW angle: %f\n\n", best_result.rot_x, best_result.rot_y, best_result.rot_z, best_result.fow);
	
	/*Calcolo runtime e benchmarking*/
	const double runtime_init_bitmap = runtime_ms(create_d_bitmap_evt);
	const double runtime_init_data = total_runtime_data;
	const double runtime_directions = runtimes.x;
	const double runtime_rotation = runtimes.y;
	const double runtime_raycast = runtimes.z;
	const double runtime_comparisons = total_runtime_comparison;

	/*Kernel diversi - benchmarking da correggere*/
	const double init_bitmap_bw = (3 * img_size * sizeof(unsigned char) * sizeof(cl_float) + img_size * sizeof(cl_bool))/ 1.0e6 / runtime_init_bitmap;
	const double init_data_bw = ((num_of_els * 3.0 * sizeof(cl_float)) + (num_of_els/3.0 * sizeof(float)))/ 1.0e6 / runtime_init_data;
	const double generate_dir_bw = num_of_casts.x * ((sizeof(cl_float3) * width * height)) / 1.0e6 / runtime_directions;
	const double apply_rotation_bw = (num_of_casts.y * 7 * sizeof(cl_float3) * width * height) / 1.0e6 / runtime_rotation;
	const double ray_cast_bw = num_of_casts.z * (sizeof(cl_float3) * width * height + sizeof(cl_bool) * img_size/channels)/ 1.0e6 / runtime_raycast;
	const double comparison_bw = num_of_comparisons * (2*(width * height * sizeof(cl_bool)) + (width * height * sizeof(cl_int))) / 1.0e6 / runtime_comparisons;

	printf("init_bitmap: %d els in %g ms %g GB/s\n", 2 * img_size, runtime_init_bitmap, init_bitmap_bw);
	printf("init_data: %lld els in %g ms %g GB/s\n", num_of_els * 4 * sizeof(float), runtime_init_data, init_data_bw);
	printf("generate_dir: %d els in %g ms %g GB/s\n", width * height, runtime_directions, generate_dir_bw);
	printf("apply rotation: %d els in %g ms %g GB/s\n", width* height, runtime_rotation/num_of_casts.y, apply_rotation_bw);
	printf("raycast: %d els in %g ms %g GB/s\n", img_size/channels, runtime_raycast/num_of_casts.z, ray_cast_bw);
	printf("comparison: %d els in %g ms %g GB/s\n", num_of_comparisons* width* height, runtime_comparisons, comparison_bw);

	clReleaseKernel(init_bitmap_k);
	clReleaseProgram(prog);
	clReleaseCommandQueue(que);
	clReleaseContext(ctx);
	clReleaseMemObject(d_data_buffer);

	return 0;
}