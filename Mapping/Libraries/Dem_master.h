#include <stdlib.h>
#include <stdio.h>
#include <cfloat>
#include <time.h>
#include <vector>
#include <utility>

#include "vectors.h"

/*Parsing DEM:kernel*/
cl_event init_data(cl_kernel data_k, cl_command_queue que, cl_mem d_vec, cl_mem data_buffer, const cl_int width, const cl_int min_north, const cl_int min_east, const cl_int size,
	cl_int gws_align_init_data, cl_event* wait) {
	const size_t gws[] = { round_mul_up(size, gws_align_init_data)};
	cl_int err;
	cl_event initdata_evt;
	cl_uint i = 0;
	err = clSetKernelArg(data_k, i++, sizeof(d_vec), &d_vec);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(data_k, i++, sizeof(data_buffer), &data_buffer);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(data_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(data_k, i++, sizeof(min_north), &min_north);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(data_k, i++, sizeof(min_east), &min_east);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(data_k, i++, sizeof(size), &size);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, data_k, 1, NULL, gws, NULL, 1, wait, &initdata_evt);
	ocl_check(err, "enqueue initdata failed");
	return initdata_evt;
}

void preparse(char* filename, int* buffer_width, int* buffer_height, int* m_north, int* m_east) {
	FILE* fp;
	fp = fopen(filename, "r");
	if (fp == NULL) {
		perror("Failed to open file. \n");
		exit(1);
	}
	else {
		printf("Preparsing file %s..\n", filename);
	}

	float min_east = 0, max_east = 0, min_north = 0, max_north = 0;

	fscanf(fp, "%f%f", &min_east, &max_north);
	fseek(fp, -1, SEEK_END);
	
	do {
		fseek(fp, -2, SEEK_CUR);
	} while (fgetc(fp) != '\n');

	fscanf(fp, "%f%f", &max_east, &min_north);

	*buffer_width = (int)(max_east - min_east+1);
	*buffer_height = (int)(max_north - min_north+1);
	*m_north = (int)min_north;
	*m_east = (int)min_east;
}

/*Parsing con .txt*/
long long load_data(char* filename, float* max_altitude, int* buffer_width, int* buffer_height, int* m_north, int* m_east, cl_mem* d_data_buffer,
	cl_mem* hd_vec, cl_context ctx, cl_command_queue que, cl_event* write_evt, cl_kernel init_data_k, cl_event* init_data_evt, double* total_runtime_data,
	int gws_align_init_data) {
	preparse(filename, buffer_width, buffer_height, m_north, m_east);

	FILE* fp;
	fp = fopen(filename, "r");
	if (fp == NULL) {
		perror("Failed to open file. \n");
		exit(1);
	}
	else {
		printf("Parsing file %s..\n", filename);
	}

	printf("Width: %d, Height : %d\n", *buffer_width, *buffer_height);
	struct vec vec;
	vector_init(&vec, 1000);
	
	long long num_of_els = 0;
	float northing, easting;
	char buffer[255];
	int err;
	bool end = false;

	/*Crea array che conterr� i dati finali*/
	*d_data_buffer = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_float) * *buffer_height * *buffer_width, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_data_buffer");

	while (!end) {
		if (fscanf(fp, "%f%f%s", &easting, &northing, buffer) == EOF) end = true;
		if (strcmp(buffer, "nan") != 0) {
			vector_push_back(&vec, easting);
			vector_push_back(&vec, northing);
			vector_push_back(&vec, (float)atof(buffer));
			if ((float)atof(buffer) > *max_altitude) *max_altitude = (float)atof(buffer);
			printf("Y: %f X: %f North: %d East: %d %ld Value: %f\n", northing, easting, (int)(northing - *m_north), (int)(easting - *m_east),
				((int)(northing - *m_north) * *buffer_width + (int)(easting - *m_east)), vec.data[vec.size-1]);
		}
		if (vec.size > 2000000 || end) {
			num_of_els += vec.size;
			/*Copia vettore su device*/
			*hd_vec = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_float) * vec.size, NULL, &err);
			err = clEnqueueWriteBuffer(que, *hd_vec, CL_TRUE, 0, sizeof(cl_float) * vec.size, vec.data, 0, NULL, write_evt);
			ocl_check(err, "errore nella creazione del buffer hd_vec");

			/*Lancia kernel inizializzazione dati*/
			*init_data_evt = init_data(init_data_k, que, *hd_vec, *d_data_buffer, *buffer_width, *m_north, *m_east, vec.size, gws_align_init_data, write_evt);
			clWaitForEvents(1, init_data_evt);
			*total_runtime_data += runtime_ms(*init_data_evt);
			free(vec.data);
			vector_init(&vec, 1000);
			clReleaseMemObject(*hd_vec);
		}
	}
	return num_of_els;
}

/*Parsing definitivo con ENVI*/
long long parse_envi(float* dem_data, float max_altitude, float buffer_width, float buffer_height, float m_south, float m_west, float m_north, float m_east, int rows, int cols, float vres, float hres,
	cl_mem* d_data_buffer, cl_mem* hd_vec, cl_context ctx, cl_command_queue que, cl_event* write_evt, cl_kernel init_data_k, cl_event* init_data_evt, double* total_runtime_data,
	int gws_align_init_data) {
	struct vec vec;
	vector_init(&vec, 1000);

	long long num_of_els = 0;
	float northing, easting;
	int err;
	bool end = false;

	/*Crea array che conterr� i dati finali*/
	*d_data_buffer = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_float) * buffer_height * buffer_width, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_data_buffer");

	double coord_x = m_west;
	double coord_y = m_north;
	float h;
	for (int r = 0; r < rows; ++r) {
		for (int c = 0; c < cols; ++c) {
			h = dem_data[r * cols + c];
			/*printf("X: %d TryX: %d Y: %d TryY: %d Index: %d TryIndex: %d\n", (int)(coord_x - m_west), (int)(c * hres), (int)(coord_y - m_south), (int)((m_north - m_south) - (r * vres)),
				(int)((int)(coord_x - m_west) * buffer_width + (int)(coord_y - m_south)), (int)((int)(c * hres) * buffer_width + (int)((m_north - m_south) - (r * vres))));*/
			//printf("R: %d C: %d TryR: %d TryC: %d\n", r, c, (int)((m_north - coord_y)/vres), (int)((coord_x - m_west)/hres));
			/*printf("Coordx: %lf Coordy: %lf Index: %d IndexDem: %d Try: %d\n", coord_x, coord_y, (int)((int)(coord_x - m_west) * buffer_width + (int)(coord_y - m_south)), (int)((r)*cols + c),
				(int)((int)(c * hres) * buffer_width + (int)((m_north - m_south) - (r * vres))));*/
			if (isfinite(h)) {
				vector_push_back(&vec, coord_x);
				vector_push_back(&vec, coord_y);
				vector_push_back(&vec, h);
			}
			coord_x += hres;
			if (coord_x > m_east) {
				coord_y -= vres;
				coord_x = m_west;
			}
		}
		if (vec.size > 200000 || r == rows - 1) {
			num_of_els += vec.size;
			/*Copia vettore su device*/
			*hd_vec = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_float) * vec.size, NULL, &err);
			err = clEnqueueWriteBuffer(que, *hd_vec, CL_TRUE, 0, sizeof(cl_float) * vec.size, vec.data, 0, NULL, write_evt);
			ocl_check(err, "errore nella creazione del buffer hd_vec");

			/*Lancia kernel inizializzazione dati*/
			*init_data_evt = init_data(init_data_k, que, *hd_vec, *d_data_buffer, buffer_width, m_south, m_west, vec.size, gws_align_init_data, write_evt);
			clWaitForEvents(1, init_data_evt);
			*total_runtime_data += runtime_ms(*init_data_evt);
			free(vec.data);
			clReleaseMemObject(*hd_vec);

			if (r != rows - 1) vector_init(&vec, 1000);
		}
	}
	return num_of_els;
}