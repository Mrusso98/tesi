#include <iostream>
#include <iomanip>
#include <chrono>
#include <cstdlib>
#include <cmath>
#include "gzstream.h"

#include "cxxenvi.hh"

#include "raster.hpp"

using namespace std;

/* Allowed characters in the first word of an ASCII grid raster */
static const char ascii_allowed[] = "abcdefghijklmnopqrtsuvwxyz:";

Raster::Raster() :
	format(UNKNOWN_FORMAT),
	rows(0), cols(0), cellcount(0),
	vres(NAN), hres(NAN),
	north(NAN), south(NAN), west(NAN), east(NAN),
	channel(),
	named_channel(),
	minmax(),
	// Default to Etna area
	projection_name("UTM"),
	projection_zone(33),
	projection_hemi("North"),
	datum("WGS-84")
{}

Raster::Raster(data_t lat1, data_t lon1, data_t lat2, data_t lon2, data_t res) : Raster()
{
	set_extent(lat1, lon1, lat2, lon2, res, res);
}

Raster::Raster(data_t lat1, data_t lon1, data_t lat2, data_t lon2, data_t vres, data_t hres) : Raster()
{
	set_extent(lat1, lon1, lat2, lon2, vres, hres);
}

Raster::Raster(data_t lat1, data_t lon1, data_t lat2, data_t lon2, index_t rows, index_t cols) : Raster()
{
	set_extent(lat1, lon1, lat2, lon2, rows, cols);
}

Raster::Raster(Raster const& org, unsigned scale) :
	format(org.format),
	rows(org.rows/scale),
	cols(org.cols/scale),
	cellcount(rows*cols),
	vres(org.vres*scale),
	hres(org.hres*scale),
	north(org.north),
	south(north - vres*rows),
	west(org.west),
	east(west + hres*cols),
	projection_name(org.projection_name),
	projection_zone(org.projection_zone),
	projection_hemi(org.projection_hemi),
	datum(org.datum)
{
	for (size_t ch = 0; ch < org.channel.size(); ++ch)
		allocate(org.channel_name(ch));
}

Raster::Raster(const char* dem_fname) : Raster()
{
	cout << "Reading raster " << dem_fname << " ..." << endl;

	const auto time_start = chrono::steady_clock::now();

	ifstream fdem(dem_fname);
	if (!fdem) printf("No dem\n");

	fdem.exceptions(ofstream::failbit | ofstream::badbit);

	// Read raster header to determine the type. We read the first string,
	// up to PEEK_MAX, to determine if it's binary or ASCII, and what kind of ASCII
#define PEEK_MAX 64

	string s;
	s.resize(PEEK_MAX+1);

	fdem >> setw(PEEK_MAX) >> s;

	if (s.find_first_not_of(ascii_allowed) == string::npos) {
		// it's an ASCII format
		//load_ascii(fdem, s);
		fdem.close();
	} else {
		// it's a binary format
		fdem.close();
		format = ENVI_BINARY;
		load_envi(dem_fname);
	}

	const auto time_end = chrono::steady_clock::now();
	const double runtime_ms = chrono::duration<double>(time_end - time_start).count()*1000;

	cout << "... raster read in " << runtime_ms << "ms" << endl;
}

void Raster::load_envi(const char *dem_fname)
{
	index_t x_point, y_point;
	data_t lat, lon;
	string units;

	auto reader = ENVI::ropen(dem_fname);
	puts("Detect binary raster with ENVI-style header");

	if (!reader->has_meta("map info")) printf("no map info found in raster header");

	std::tie(rows, cols) = reader->extent();
	cellcount = rows*cols;

	/* Get map info */
	reader->get_meta_tuple("map info",
		projection_name,
		x_point, y_point, lon, lat, hres, vres,
		projection_zone, projection_hemi, datum, units);

	if (!units.empty()) printf("units specification not supported");

	cout << "Projection: " << projection_name << " " << projection_zone << projection_hemi << ", datum: " << datum << endl;

	/* Now compute north, south, east and west */
	west = lon - (x_point - 1)*hres;
	east = lon + (cols - x_point + 1)*hres;

	north = lat + (y_point - 1)*vres;
	south = lat - (rows -y_point + 1)*vres;

	cout	<< setprecision(16)
		<< "Raster north " << north
		<< " south " << south
		<< " west " << west
		<< " east " << east
		<< ", resolution " << vres << "/" << hres << "\n"
		<< rows << "x" << cols << "=" << cellcount << " cells" << endl;

	auto const& chnames = reader->channel_names();

	for (size_t ch = 0; ch < chnames.size(); ++ch) {
		data_t *dem_z = allocate(chnames[ch]).get();

		reader->get_channel(ch, dem_z);

		// Find min/max. Is this actually used anywhere?
		data_t min_h(NAN), max_h(NAN);
		for (index_t c = 0; c < cellcount; ++c) {
			data_t h = dem_z[c];
			min_h = fmin(h, min_h);
			max_h = fmax(h, max_h);
		}
		minmax.push_back(make_pair(min_h, max_h));
	}
}

void Raster::set_extent(data_t lat1, data_t lon1, data_t lat2, data_t lon2, data_t _vres, data_t _hres)
{
	north = fmax(lat1, lat2);
	south = fmin(lat1, lat2);
	east = fmax(lon1, lon2);
	west = fmin(lon1, lon2);

	vres = _vres;
	hres = _hres;

	rows = (north - south)/vres;
	cols = (east - west)/hres;

	cellcount = rows*cols;
}

void Raster::set_extent(data_t lat1, data_t lon1, data_t lat2, data_t lon2, index_t _rows, index_t _cols)
{
	north = fmax(lat1, lat2);
	south = fmin(lat1, lat2);
	east = fmax(lon1, lon2);
	west = fmin(lon1, lon2);

	rows = _rows;
	cols = _cols;
	cellcount = rows*cols;

	vres = (north - south)/rows;
	hres = (east - west)/cols;
}

void Raster::save(std::string const& out_fname, std::string const& desc)
{
	auto out = ENVI::create<data_t>(out_fname, desc, rows, cols);
	for (size_t ch = 0; ch < channel.size(); ++ch)
		out->add_channel(channel_name(ch), get(ch));
	out->add_meta("map info", projection_name, 1, 1, west, north, hres, vres,
		projection_zone, projection_hemi, datum);

}

std::string const& Raster::channel_name(size_t chnum) const
{
	if (chnum >= channel.size())
		throw invalid_argument("no such channel number");
	for (auto const& k : named_channel)
	{
		if (k.second == chnum)
			return k.first;
	}
	throw runtime_error("channel not found?");
}

std::shared_ptr<data_t>& Raster::allocate(std::string const& name)
{
	shared_ptr<data_t> gen(new data_t[cellcount]);
	channel.push_back(move(gen));

	// make up a name if nothing was given
	string n = name;

	if (n.empty()) {
		stringstream ns;
		ns << "HEIGHT";
		if (channel.size() > 1)
			ns << channel.size();
	}

	named_channel[n] = channel.size() - 1;

	return channel.back();
}

Raster& Raster::refine(unsigned scale)
{
	index_t newcols = cols*scale;

	for (size_t ch = 0; ch < channel.size(); ++ch)
	{
		const data_t *old_z = get(ch);
		data_t *new_z = new data_t[cellcount*scale*scale];

		for (index_t r = 0; r < rows; ++r)
		{
			for (index_t c = 0; c < cols; ++c)
			{
				data_t val = old_z[r*cols + c];

				index_t new_base = r*scale*newcols + (c*scale);

				for (index_t subr = 0; subr < scale; ++subr)
				{
					for (index_t subc = 0; subc < scale; ++subc)
						new_z[ new_base + (subr*newcols) + subc ] = val;
				}
			}
		}

		channel[ch].reset(new_z);
	}

	rows *= scale;
	cols = newcols;
	cellcount = rows*cols;
	vres /= scale;
	hres /= scale;

	return *this;
}
