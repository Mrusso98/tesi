#ifndef RASTER_HPP
#define RASTER_HPP

#include <map>
#include <memory>
#include <vector>

typedef float data_t;
typedef int index_t;

struct Raster
{
	enum Format {
		UNKNOWN_FORMAT,
		GRASS_ASCIIGRID,
		ARCINFO_ASCIIGRID,
		ENVI_BINARY, // Binary file + ENVI header
	};

	Format format;

	// numer of rows and columns and cells
	index_t rows, cols, cellcount;
	// vertical and horizontal resolution
	data_t vres, hres;
	// extent
	data_t north, south, west, east;
	// array(s) of per-cell data
	std::vector<std::shared_ptr<data_t>> channel;
	std::map<std::string, size_t> named_channel;
	// data extrema
	std::vector<std::pair<data_t, data_t>> minmax;

	std::string projection_name; // Name of the projection (e.g. UTM)
	index_t projection_zone; // Zone of the projection (e.g. 33)
	std::string projection_hemi; // Hemisphere (North or South)
	std::string datum; // Datum (e.g. WGS-84)

	Raster(); // naive constructor
	// Define a new empty raster spanning the given lat/lon area with the given resolution
	Raster(data_t lat1, data_t lon1, data_t lat2, data_t lon2, data_t res);
	Raster(data_t lat1, data_t lon1, data_t lat2, data_t lon2, data_t vres, data_t hres);
	// Define a new empty raster spanning the given lat/lon area with the given
	// number of rows and columns
	Raster(data_t lat1, data_t lon1, data_t lat2, data_t lon2, index_t rows, index_t cols);
	Raster(Raster const& org, unsigned scale=1); // copy + scaling
	explicit Raster(const char *dem_fname);
	explicit Raster(std::string const& dem_fname) : Raster(dem_fname.c_str()) {}

	~Raster()
	{
		channel.clear();
		named_channel.clear();
	}

	void save(std::string const& out_fname, std::string const& desc);

	std::string const& channel_name(size_t chnum) const;

	// allocate a new dataset for this raster, with an optional name
	std::shared_ptr<data_t>& allocate(std::string const& name="");

	// get raw channel data, by index
	data_t *get(size_t chnum=0)
	{ return channel.at(chnum).get(); }
	const data_t *get(size_t chnum=0) const
	{ return channel.at(chnum).get(); }

	// get raw channel data for the given channel name
	data_t *get(std::string const& name)
	{ return get(named_channel.at(name)); }
	const data_t *get(std::string const& name) const
	{ return get(named_channel.at(name)); }

	// Refine a Raster (increasing the resolution by the given scale)
	Raster& refine(unsigned scale=2/* method = CONSTANT /// TODO */);

private:
	void set_extent(data_t lat1, data_t lon1, data_t lat2, data_t lon2, data_t _vres, data_t _hres);
	void set_extent(data_t lat1, data_t lon1, data_t lat2, data_t lon2, index_t rows, index_t cols);

	void load_ascii(std::istream&, std::string const&);
	void load_envi(const char *dem_fname);
};

#endif
