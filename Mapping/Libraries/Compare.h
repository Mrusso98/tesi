#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

typedef struct {
	float focal_point;
	cl_float3 origin;
	cl_float3 rotation;
	float fow;
} Camera;

void CamInit(Camera* cam, float _focal, cl_float3 _origin, cl_float3 _rotation, float _fow) {
	cam->focal_point = _focal;
	cam->origin = _origin;
	cam->rotation = _rotation;
	cam->fow = _fow;
}

void compare_data_buffer(float* d_A, float* d_B, int d_size) {
	for (int i = 0; i < d_size; i++) {
		if (d_A[i] != d_B[i] && (!(d_A[i] == 0 && d_B[i] == -431602080.000000))) {
			printf("Errore rilevato nel confronto fra i data buffer!\n");
			printf("%f != %f all'indice %d\n", d_A[i], d_B[i], i);
			exit(1);
		}
	}
}

void compare_bitmap(bool* bit_A, bool* bit_B, int bit_size) {
	for (int i = 0; i < bit_size; i++) {
		if (bit_A[i] != bit_B[i]) {
			printf("Errore rilevato nel confronto fra le bitmap!");
			exit(1);
		}
	}
}

/*Copia i dati su host e procedi a confrontarli*/
/*float* dh_data_buffer = (float*)malloc(sizeof(float) * vec_height * vec_width);
err = clEnqueueReadBuffer(que, d_data_buffer, CL_TRUE, 0, sizeof(cl_float) * vec_height * vec_width, dh_data_buffer, 1, &init_data_evt, &read_evt);
compare_data_buffer(dh_data_buffer, data_buffer, vec_height* vec_width);*/

/*Copia risultato su host per confronto*/
/*bool* dh_bitmap = (bool*)malloc(sizeof(bool) * img_size / channels);
err = clEnqueueReadBuffer(que, d_bitmap, CL_TRUE, 0, sizeof(bool) * img_size / channels, dh_bitmap, 1, &create_d_bitmap_evt, &read_evt);
ocl_check(err, "errore nella creazione del buffer dh_bitmap");
compare_bitmap(dh_bitmap, h_bitmap, img_size / channels);*/

/*Parsing del DEM su host*/
/*float* data_buffer;
int buffer_width, buffer_height, min_north, min_east;
data_buffer = load_3D_data("Eserc.txt", &buffer_width, &buffer_height);*/

/*Crea la bitmap su host*/
/*bool* h_bitmap = create_bitmap(img, img_size, channels);
unsigned char* img_jpeg_2 = bitmap_to_jpeg(h_bitmap, img_size / channels, channels);
stbi_write_jpg("img_bitmap_2.jpg", width, height, channels, img_jpeg_2, 100);*/