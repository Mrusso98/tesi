#define CL_TARGET_OPENCL_VERSION 120
#define PI 3.1415
#define ROT_CONST 57.2888f

#include "libraries/ocl_boiler.h" //Boilerplate OpenCL
#include "libraries/Dem_master.h" //Effettua parsing del file DEM
#include "libraries/Jpg_master.h" //Legge e scrive immagini, gestisce le bitmap
#include "libraries/Camera.h" //Effettua confronti fra risultati host e device
#include "libraries/Nelder_Mead.h" //Funzioni helper per Nelder-Mead
#include "libraries/raster.h"

size_t gws_align_init_data;
size_t gws_align_direction;
size_t gws_align_rotation;
size_t gws_align_points;
size_t gws_align_ray;

typedef struct {
	cl_float4 result;
	cl_float3 origins;
	char dem_filename[256];
	char image_name[256];
}exec_settings;

/*Legge file Settings.txt, inizializza parametri*/
void exec_settings_init(exec_settings* my_settings) {
	FILE* fp;
	fopen_s(&fp, "Settings.txt", "r");

	if (fp == NULL) {
		printf("File Settings.txt missing from directory.\n");
		exit(1);
	}

	int count = 0;
	char line[256];
	while (fgets(line, 256, fp)) {
		if (count == 0)	fscanf(fp, "%f %f %f %f", &my_settings->result.s0, &my_settings->result.s1, &my_settings->result.s2, &my_settings->result.s3);
		if (count == 3) fscanf(fp, "%f %f %f", &my_settings->origins.x, &my_settings->origins.y, &my_settings->origins.z);
		if (count == 6) fscanf(fp, "%s", my_settings->dem_filename);
		if (count == 9)	fscanf(fp, "%s", my_settings->image_name);
		count++;
	}
}

/*Genera vettori direzione*/
cl_event generate_dir(cl_kernel dir_k, cl_command_queue que, cl_mem d_directions, cl_float focal, const cl_int width, const cl_int height, const cl_double fow_tan, const cl_float aspect_ratio) {
	const size_t gws[] = { round_mul_up(width, gws_align_direction), round_mul_up(height, gws_align_direction) };

	cl_int err;
	cl_event direction_evt;
	cl_uint i = 0;
	err = clSetKernelArg(dir_k, i++, sizeof(d_directions), &d_directions);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(focal), &focal);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(fow_tan), &fow_tan);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(dir_k, i++, sizeof(aspect_ratio), &aspect_ratio);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, dir_k, 2, NULL, gws, NULL, 0, NULL, &direction_evt);
	ocl_check(err, "enqueue generate_dir failed");
	return direction_evt;
}

/*Normalizza vettori direzione*/
cl_event normalize(cl_kernel norm_k, cl_command_queue que, cl_mem d_directions, const cl_int width, const cl_int height) {
	const size_t gws[] = { round_mul_up(width, gws_align_direction), round_mul_up(height, gws_align_direction) };

	cl_int err;
	cl_event normalize_evt;
	cl_uint i = 0;
	err = clSetKernelArg(norm_k, i++, sizeof(d_directions), &d_directions);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(norm_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(norm_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, norm_k, 2, NULL, gws, NULL, 0, NULL, &normalize_evt);
	ocl_check(err, "enqueue normalize failed");
	return normalize_evt;
}

/*Applica matrici di rotazione al vettore*/
cl_event apply_rotation(cl_kernel rot_k, cl_command_queue que, cl_mem d_directions, const cl_int width, const cl_int height, const cl_float3 sin, const cl_float3 cos) {
	const size_t gws[] = { round_mul_up(width, gws_align_rotation), round_mul_up(height, gws_align_rotation) };
	cl_int err;
	cl_event rotation_evt;
	cl_uint i = 0;

	err = clSetKernelArg(rot_k, i++, sizeof(d_directions), &d_directions);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(sin), &sin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(rot_k, i++, sizeof(cos), &cos);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, rot_k, 2, NULL, gws, NULL, 0, NULL, &rotation_evt);
	ocl_check(err, "enqueue apply_rotation failed");
	return rotation_evt;
}

/*Trova coefficienti angolari iniziali*/
cl_event find_start_points(cl_kernel points_k, cl_command_queue que, cl_float3 origin, cl_mem direction, cl_mem start_points, cl_int width, cl_int height,
	cl_int buf_width, cl_int buf_height, cl_float max_altitude) {
	const size_t gws[] = { round_mul_up(width, gws_align_points), round_mul_up(height, gws_align_points) };
	cl_int err;
	cl_event points_evt;
	cl_uint i = 0;
	err = clSetKernelArg(points_k, i++, sizeof(origin), &origin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(direction), &direction);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(start_points), &start_points);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(buf_width), &buf_width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(buf_height), &buf_height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(points_k, i++, sizeof(max_altitude), &max_altitude);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, points_k, 2, NULL, gws, NULL, 0, NULL, &points_evt);
	ocl_check(err, "enqueue start_points failed");
	return points_evt;
}

cl_event ray_map(cl_kernel map_k, cl_command_queue que, cl_float3 origin, cl_mem direction, cl_mem map, cl_mem data_buffer, cl_int width, cl_int height,
	cl_int buffer_width, cl_int buffer_height, cl_float max_altitude, cl_mem start_points) {
	const size_t gws[] = { round_mul_up(width, gws_align_ray), round_mul_up(height, gws_align_ray) };
	cl_int err;
	cl_event map_evt;
	cl_uint i = 0;
	err = clSetKernelArg(map_k, i++, sizeof(origin), &origin);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(direction), &direction);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(map), &map);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(data_buffer), &data_buffer);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(width), &width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(height), &height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(buffer_width), &buffer_width);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(buffer_height), &buffer_height);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(max_altitude), &max_altitude);
	ocl_check(err, "set parameter data failed", i - 1);
	err = clSetKernelArg(map_k, i++, sizeof(start_points), &start_points);
	ocl_check(err, "set parameter data failed", i - 1);

	err = clEnqueueNDRangeKernel(que, map_k, 2, NULL, gws, NULL, 0, NULL, &map_evt);
	ocl_check(err, "enqueue ray_map failed");
	return map_evt;
}

void pre_casting(cl_command_queue que, cl_context ctx, cl_kernel generate_dir_k, cl_kernel normalize_k, cl_kernel rotation_k, cl_kernel start_points_k, cl_mem d_data_buffer,
	cl_mem d_directions, cl_mem d_start_points, int vec_width, int vec_height, int width, int height, float max_altitude, Camera my_cam) {
	cl_event read_evt;
	int err;
	cl_float3 _sin;
	cl_float3 _cos;

	/*Genera vettori direzione*/
	const cl_double fow_tan = tan((my_cam.fow / 2.0) * PI / 180.0);
	const cl_float aspect_ratio = width / (float)height;
	cl_event generate_dir_evt = generate_dir(generate_dir_k, que, d_directions, my_cam.focal_point, width, height, fow_tan, aspect_ratio);
	clWaitForEvents(1, &generate_dir_evt);

	/*Applica rotazione*/
	_sin.x = sin(my_cam.rotation.x / ROT_CONST);
	_cos.x = cos(my_cam.rotation.x / ROT_CONST);
	_sin.y = sin(my_cam.rotation.y / ROT_CONST);
	_cos.y = cos(my_cam.rotation.y / ROT_CONST);
	_sin.z = sin(my_cam.rotation.z / ROT_CONST);
	_cos.z = cos(my_cam.rotation.z / ROT_CONST);
	cl_event apply_rotation_evt = apply_rotation(rotation_k, que, d_directions, width, height, _sin, _cos);
	clWaitForEvents(1, &apply_rotation_evt);

	/*Normalizza*/
	cl_event normalize_evt = normalize(normalize_k, que, d_directions, width, height);
	clWaitForEvents(1, &normalize_evt);

	/*Calcola coefficienti angolari*/
	cl_event start_points_evt = find_start_points(start_points_k, que, my_cam.origin, d_directions, d_start_points, width, height, vec_width, vec_height, max_altitude);
	clWaitForEvents(1, &start_points_evt);

	err = clFinish(que);
	clReleaseEvent(apply_rotation_evt);
	clReleaseEvent(normalize_evt);
	clReleaseEvent(start_points_evt);
}

cl_int2* mapping(cl_command_queue que, cl_context ctx, cl_kernel generate_dir_k, cl_kernel normalize_k, cl_kernel rotation_k, cl_kernel start_points_k, cl_kernel ray_map_k,
	cl_mem d_data_buffer, cl_mem d_directions, cl_mem d_start_points, int vec_width, int vec_height, int width, int height, float max_altitude, Camera my_cam) {
	cl_int err;
	cl_event read_evt;

	pre_casting(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, d_data_buffer, d_directions, d_start_points, vec_width, vec_height, width, height, max_altitude,
		my_cam);

	/*Ray casting*/
	cl_mem d_result_map = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int2) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_result_map");
	cl_event ray_map_evt = ray_map(ray_map_k, que, my_cam.origin, d_directions, d_result_map, d_data_buffer, width, height, vec_width, vec_height, max_altitude, d_start_points);
	clWaitForEvents(1, &ray_map_evt);

	cl_int2* dh_result_map = (cl_int2*)malloc(sizeof(cl_int2) * width * height);
	err = clEnqueueReadBuffer(que, d_result_map, CL_TRUE, 0, sizeof(cl_int2) * width * height, dh_result_map, 0, NULL, &read_evt);
	ocl_check(err, "errore nella creazione del buffer dh_result_map");

	clReleaseMemObject(d_result_map);
	clReleaseEvent(ray_map_evt);
	return dh_result_map;
}

int main() {
	exec_settings my_settings;
	exec_settings_init(&my_settings);

	cl_platform_id p;
	cl_device_id d;
	cl_context ctx;
	cl_command_queue que;
	cl_program prog;

	/*Boilerplate OCL*/
	p = select_platform(0);
	d = select_device(p);
	ctx = create_context(p, d);
	que = create_queue(ctx, d);
	prog = create_program("CameraMap.ocl", ctx, d);

	cl_int err;

	/*Inizializza i kernel del codice device*/
	cl_kernel init_data_k = clCreateKernel(prog, "init_data", &err);
	ocl_check(err, "errore nella creazione del kernel init_bitmap");
	cl_kernel generate_dir_k = clCreateKernel(prog, "generate_dir", &err);
	ocl_check(err, "errore nella creazione del kernel generate_dir");
	cl_kernel normalize_k = clCreateKernel(prog, "normalize_dir", &err);
	ocl_check(err, "errore nella creazione del kernel normalize");
	cl_kernel rotation_k = clCreateKernel(prog, "apply_rotation", &err);
	ocl_check(err, "errore nella creazione del kernel apply_rotation");
	cl_kernel start_points_k = clCreateKernel(prog, "find_start", &err);
	ocl_check(err, "errore nella creazione del kernel start_points");
	cl_kernel ray_map_k = clCreateKernel(prog, "ray_map", &err);
	ocl_check(err, "errore nella creazione del kernel ray_map");

	/*Query sul device per ottenere i preferred work group size multiple di ogni kernel*/
	err = clGetKernelWorkGroupInfo(init_data_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_init_data), &gws_align_init_data, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(generate_dir_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_direction), &gws_align_direction, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(rotation_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_rotation), &gws_align_rotation, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(start_points_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_points), &gws_align_points, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");
	err = clGetKernelWorkGroupInfo(ray_map_k, d, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(gws_align_ray), &gws_align_ray, NULL);
	ocl_check(err, "errore nel calcolo del preferred work group size multiple");

	Raster dem(my_settings.dem_filename);
	float* dem_data = dem.get();

	Camera my_cam;

	/*Carica dati dell'immagine .jpg. Il nome � passato tramite parametro.*/
	int width, height, channels;
	unsigned char* img = load_img(my_settings.image_name, &width, &height, &channels);
	size_t img_size = width * height * channels;

	cl_float3 new_rot = { my_settings.result.s0, my_settings.result.s1, my_settings.result.s2 };
	cl_float3 origins = { my_settings.origins.x - dem.west, my_settings.origins.y - dem.south, my_settings.origins.z };
	CamInit(&my_cam, -1, origins, new_rot, my_settings.result.s3);

	cl_mem d_data_buffer, hd_vec;
	cl_event write_evt, init_data_evt;

	float vec_width = dem.east - dem.west + 1;
	float vec_height = dem.north - dem.south + 1;
	float max_altitude = dem.minmax[0].second;
	double total_runtime_data = 0;

	long long num_of_els = parse_envi(dem_data, max_altitude, vec_width, vec_height, dem.south, dem.west, dem.north, dem.east, dem.rows, dem.cols, dem.vres, dem.hres,
		&d_data_buffer, &hd_vec, ctx, que, &write_evt, init_data_k, &init_data_evt, &total_runtime_data, gws_align_init_data);

	/*Crea buffer OpenCL necessari per l'esecuzione*/
	cl_mem d_directions = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_float3) * img_size / channels, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_directions");

	cl_mem d_start_points = clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_double) * width * height, NULL, &err);
	ocl_check(err, "errore nella creazione del buffer d_start_points");

	cl_int2* new_data = mapping(que, ctx, generate_dir_k, normalize_k, rotation_k, start_points_k, ray_map_k, d_data_buffer, d_directions, d_start_points,
		vec_width, vec_height, width, height, max_altitude, my_cam);
	
	clReleaseMemObject(d_directions);
	clReleaseMemObject(d_start_points);
	clReleaseMemObject(d_data_buffer);

	Raster rast = dem;
	float** data = (float**)malloc(sizeof(float*) * 4);
	data[0] = rast.get();
	data[1] = rast.allocate("red").get(); 
	data[2] = rast.allocate("green").get();
	data[3] = rast.allocate("blue").get();
	
	for (int i = 0; i < dem.rows; i++) {
		for (int j = 0; j < dem.cols; j++) {
			data[0][i * dem.cols + j] = nanf("");
			data[1][i * dem.cols + j] = nanf("");
			data[2][i * dem.cols + j] = nanf("");
			data[3][i * dem.cols + j] = nanf("");
		}
	}

	int coeff_x, coeff_y;
	for (int i = 0; i < width * height; i++) {
		coeff_y = (dem.north - (dem.south + new_data[i].y)) / dem.vres;
		coeff_x = new_data[i].x / dem.hres;
		data[0][coeff_y * dem.cols + coeff_x] = dem_data[coeff_y * dem.cols + coeff_x];
		data[1][coeff_y * dem.cols + coeff_x] = img[channels * i + 0];
		data[2][coeff_y * dem.cols + coeff_x] = img[channels * i + 1];
		data[3][coeff_y * dem.cols + coeff_x] = img[channels * i + 2];
	}

	rast.save("map.bsq", "mapping raster");

	return 0;
}